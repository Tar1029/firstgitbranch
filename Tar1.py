# -*- coding: utf-8 -*-
import os
import pygame
from pygame import *


class Unit:
    def __init__(self, pos):
        self.image = pygame.image.load(os.path.join("Images", "buck.png"))
        self.image1 = pygame.image.load(os.path.join("Images", "drop.png"))
        self.x = pos[0]
        self.y = pos[1]
        self.action = "stop"


    def event_handler(self,event):
        if event.type == pygame.KEYUP:
            self.action = "stop"
            return
        if event.key == pygame.K_LEFT:
            self.action = "left"
        elif event.key == pygame.K_RIGHT:
            self.action = "right"




    def event_handler2(self):
            self.action = "down"



    def move(self):
        if self.action == "left" and self.x>=0:
            self.x -= 10
        if self.action == "right" and self.x<=770:
            self.x += 10
        if self.action == "down" and self.y >= -128:
            self.y -=10

    # print(event.key)

    # TODO: Доработку Класса Unit выполняем вместе с преподавателем

    def update(self):
       self.move()


FPS = 30
pygame.init()  # инициализация
display = pygame.display.set_mode((900, 900))  # создание окна
screen = pygame.display.get_surface()  # определяем поверхность для рисования

buck = Unit((0, 585))
drop = Unit((400,0))
done = False
clock = pygame.time.Clock()
while not done:  # главный цикл программы
    for e in pygame.event.get():  # цикл обработки очереди событий окна
        if e.type == pygame.QUIT:  # Обработка события "Закрытие окна"
            done = True

        if e.type == pygame.KEYDOWN:
            buck.event_handler(e)
        if e.type == pygame.KEYUP:
            buck.event_handler(e)
    buck.update()
    drop.update()
    screen.fill((0, 100, 100)) # Заливаем экран цветом
    screen.blit(buck.image,(buck.x, buck.y))  # отрисовываем содержимое поверхности
    pygame.display.flip()  # показываем на экране все что нарисовали на основной поверхности
    screen.fill((0, 100, 100)) # Заливаем экран цветом
    screen.blit(drop.image,(drop.x, drop.y))  # отрисовываем содержимое поверхности
    clock.tick(FPS)